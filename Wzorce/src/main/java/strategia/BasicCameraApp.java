package strategia;

public class BasicCameraApp extends PhoneCameraApp {

    public BasicCameraApp(SharingModule sharingModule) {
        super(sharingModule);
    }

    @Override
    public void edit() {
        System.out.println("Editing with BasicCameraApp");
    }
}
