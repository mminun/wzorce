package strategia;

public class CameraPlusApp extends PhoneCameraApp {

    public CameraPlusApp(SharingModule sharingModule) {
        super(sharingModule);
    }

    @Override
    public void edit() {
        System.out.println("editing with CameraPlusApp");
    }
}
