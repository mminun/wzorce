package strategia;

public abstract class PhoneCameraApp {
private SharingModule sharingModule;

  public PhoneCameraApp(SharingModule sharingModule) {
    this.sharingModule = sharingModule;
  }

  public void setSharingModule(SharingModule sharingModule) {
    this.sharingModule = sharingModule;
  }

  public void take(){}

  public abstract void edit();
  public void save(){}
  public void share(){
    sharingModule.share();
  }

}
