package adapter;

public class smallDrone implements Drone {
    @Override
    public void beep() {
        System.out.println("beep beep");
    }

    @Override
    public void spin_rotors() {
        System.out.println("rotors are spinning");
    }

    @Override
    public void take_off() {
        System.out.println("taking off");
    }
}
