package adapter;

public class MallardDuck implements Duck {
    @Override
    public void quack() {
        System.out.println("kwacze");
    }

    @Override
    public void fly() {
        System.out.println("leci");
    }
}
