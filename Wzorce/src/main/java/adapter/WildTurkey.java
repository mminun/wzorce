package adapter;

public class WildTurkey implements Turkey{
    @Override
    public void gobble() {
        System.out.println("gul gul");
    }

    @Override
    public void fly() {
        System.out.println("leci turkey");
    }
}
