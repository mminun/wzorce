package adapter;

public class DroneAdapter implements Duck {

    public DroneAdapter(Drone drone) {
        this.drone = drone;
    }

    Drone drone;

    @Override
    public void quack() {
        drone.beep();
    }

    @Override
    public void fly() {
        drone.spin_rotors();
        drone.take_off();
    }
}
